---
title: Chad Retz - Resume
layout: default
---

# Chad Retz
* <chad.retz@gmail.com>
* 817-917-4256

#### Background

Extremely motivated developer with a very strong emphasis towards code quality, accuracy, and productivity. Looking for position to challenge myself while allowing innovation and development of new technologies.

I create and contribute to many open source projects. I also maintain an up-to-date knowledge on all things software and constantly learn new languages and approaches.

## Experience

### RealPage, Inc.
#### Sr. Software Engineer - February 2008 to present

Developed browser-based bridge as an extension to facillitate native access for RealPage websites. Primarily developed in C# as an Internet Explorer BHO.

Engineered internal portal for employee use. Utilized Seam and IceFaces for a full AJAX environment. Also made heavy use of geo-spatial technologies (e.g. GeoTools) to support the geographical side of RealPage's business.

Developed web-based business intelligence dashboard for customers. Dashboard is highly interactive with several maps and widgets. Heavy use of GWT.

Primary ETL developer which specialized in obtaining data from dozens of disparate data sources and consolidating into a common, clean location. The main challenge was maintaining a high level of scalability and performance regardless of the amount of data. Utilizing grid computing libraries, I was able to obtain and transform hundreds of millions of rows of data in a few minutes.

As department lead for RealPage's Business Intelligence development group, I managed the version control system, project management tools, releases, and overall project architecture beyond my normal development duties.

### The Planet
#### Software Engineer - Mid 2005 to February 2008

Primary developer on network abstraction API. Beyond the full implementation of switch/router communication across different equipment vendors, I also helped manage the deprecation of the legacy systems which were no longer adequate to handle the infrastructure. This was developed in pure Java.

Main developer on frontend website for the four websites The Planet maintained at that time. I built the layout engine, shopping carts, and persistable server configurations. Frontend was Struts/Tiles, backend was EJB3/Hibernate.

### Personal
#### Github profile - [https://github.com/cretz](https://github.com/cretz)

In response to a request on the node.js mailing list, I developed a pure JavaScript implementation of the TDS protocol for native access to SQL Server databases. Project was developed in CoffeeScript and can be seen at [https://github.com/cretz/node-tds](https://github.com/cretz/node-tds).

Developed GWT-frontend for node.js implementation. This implementation is liked by many for the fact that it allows one to use all their Java tooling and refactoring ability instead of using the highly-dynamic JavaScript language. Project was developed in GWT and can be seen at [https://github.com/cretz/gwt-node](https://github.com/cretz/gwt-node).

Back in 2009 a contest was launched to build the best artificial intelligence engine for StarCraft. To make this easy for Java developers, I wrapped the existing C++ API into a friendly API for others to develop their contest entries. Multiple users used this wrapper for the competition, but it has since been deprecated. Project was developed in C++ and Java and can be seen at [http://code.google.com/p/bwapi-jbridge/](http://code.google.com/p/bwapi-jbridge/).

## Technologies

### Java

Strong knowledge of Java at all levels and knowledge of several languages that run on the JVM (e.g. Scala, Groovy, etc). Primary strength is in extremely high performing backend systems that scale horizontally and communicate with each other.

I have used almost every major Java library/framework at some point (too numerous to list here) and have a strong familiarity with best practices.

### PHP

Strong knowledge of PHP 5 and proper OO design. I am familiar with the Zend Framework and templating engines such as Smarty.

### JavaScript/CoffeeScript

Been developing server-side components in node.js for almost 2 years primarily in CoffeeScript. I have also developed in frontend JavaScript/CSS for several interactive web applications. I have extensive knowledge in the common technologies utilized such as jQuery, Express, Mocha, Jade, Backbone, Underscore, etc.

### Database

I am very familiar with several traditional SQL databases such as Oracle, SQL Server, MySQL, and Postgres. Many previous applications I have built required extensive schema development and query writing.

I am also familiar with non-traditional databases such as MongoDB and Redis. Although I have not used these in a professional setting, I have used them in personal projects.

### Other

I have a strong background in C# and although I don't use it on a daily basis, I have written several .Net applications.

I have built and maintained many web services. Most of which have utilized SOAP, but some have been RESTful.

I have used several types of server applications including web servers (e.g. httpd, Tomcat, nginx) and message queue servers (e.g. HornetQ, RabbitMQ, and ActiveMQ). I also have knowledge in both Linux and Windows operating systems and have developed many applications for both (and many cross-platform applications).

## Education

University of Texas at Dallas 2002 - 2004 – studied Computer Science